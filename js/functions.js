const dividendos = [8000, 400, 20, 1];

const conversionArabicToMaya = () => {
    let response = [];
    let number = document.getElementById('arabigo').value;

    for (let index = 0; index < 4; index++) {
        document.getElementById('img'+ index).setAttribute('src', '')
    }
    if (number < 99999) {
        if (number != '') {
            if (number == 0) {
                response.push(0);
            } else {
                for (dividendo of dividendos) {
                    let integers = parseInt(number / dividendo);
                    if (integers) {
                        response.push(integers);
                        number -= (integers * dividendo);
                    } else {
                        response.push(0);
                    }
                }
            
            }
        
            let position = 0;
            for (const number of response) {
                document.getElementById('img'+ position).setAttribute('src', 'img/' + number +'.png')
                position++;
            }
        
            createRegistry(response, 'Arábigo a Maya');
        }  
    } else {
        alert('Ingresa un numero valido');
    }
    return response;
}

const conversionMayaToArabic = () => {
    let response = 0, position = 0;
    let simbols = [];
    simbols.push(positionMaya['4'])
    simbols.push(positionMaya['3'])
    simbols.push(positionMaya['2'])
    simbols.push(positionMaya['1'])
    for (simbol of simbols) {
        response += simbol * dividendos[position];
        position++;
    }
    createRegistry(response, 'Maya a Arábigo');
    document.getElementById('maya').setAttribute('value', response)
    return response;
}

const createRegistry = (simbols, type) => {
    let registry = JSON.parse(localStorage.getItem('registry'));
    if (!registry) registry = []
    registry.push({
        'simbols': simbols,
        'type': type,
        'time': moment().format()
    });
    localStorage.setItem('registry', JSON.stringify(registry))
}

const bringRegistry = () => {
    return JSON.parse(localStorage.getItem('registry'));
}


function generateSelect(){
    let html = '';
    for (let index = 0; index < 20; index++) {
        html += `<li><a class="dropdown-item" onclick="setImg('imgSelect0', ${index}, 1)"><img src="../img/${index}.png" id="img${index}" width="50px"></a></li>`;
    }
    document.getElementById('select0').innerHTML = html;
    
    html = ''
    for (let index = 0; index < 20; index++) {
        html += `<li><a class="dropdown-item" onclick="setImg('imgSelect1', ${index}, 2)"><img src="../img/${index}.png" id="img${index}" width="50px"></a></li>`;
    }
    document.getElementById('select1').innerHTML = html;

    html = ''
    for (let index = 0; index < 20; index++) {
        html += `<li><a class="dropdown-item" onclick="setImg('imgSelect2', ${index}, 3)"><img src="../img/${index}.png" id="img${index}" width="50px"></a></li>`;
    }
    document.getElementById('select2').innerHTML = html;

    html = ''
    for (let index = 0; index < 20; index++) {
        html += `<li><a class="dropdown-item" onclick="setImg('imgSelect3', ${index}, 4)"><img src="../img/${index}.png" id="img${index}" width="50px"></a></li>`;
    }
    document.getElementById('select3').innerHTML = html;
}

let positionMaya = {
    1: 0,
    2: 0,
    3: 0,
    4: 0
}
function setImg(select, img, level){
    document.getElementById(select).setAttribute('src', 'img/' + img +'.png')
    positionMaya[level] = img;
}
